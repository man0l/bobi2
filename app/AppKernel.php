<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
        		
        	new Response\PageBundle\ResponsePageBundle(),
        	new Response\HomeBundle\ResponseHomeBundle(),
        	new Response\SecurityBundle\ResponseSecurityBundle(),
        	new FOS\UserBundle\FOSUserBundle(),
        	new FOS\RestBundle\FOSRestBundle(),
        	new JMS\SerializerBundle\JMSSerializerBundle(),
        	new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
        		
        		
        	new Response\UserBundle\ResponseUserBundle(),
        	new Response\ProductBundle\ResponseProductBundle(),
        	new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
        	new Response\APIBundle\ResponseAPIBundle(),
        	new Response\OrdersBundle\ResponseOrdersBundle(),
            new Response\FrontBundle\ResponseFrontBundle(),
        		
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
