<?php

namespace Response\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ResponseUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
