<?php 
	
namespace Response\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Response\OrdersBundle\Entity\Orders as Orders;
use FOS\UserBundle\Model\GroupableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser {
	
	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;
	
	/**
	 * @ORM\Column(type="string", name="name", length=255, nullable=true)
	 * @var unknown
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="string", name="phone", length=255, nullable=true)
	 * @var unknown
	 */
	protected $phone;
	
	/**
	 * @ORM\Column(type="string", name="city", length=255, nullable=true)
	 * @var unknown
	 */
	protected $city;
	
	/**
	 * @ORM\Column(type="string", name="address", length=255, nullable=true)
	 * @var unknown
	 */
	protected $address;
	
	/**
	 * @ORM\OneToMany(targetEntity="Response\OrdersBundle\Entity\Orders", mappedBy="user")
	 * @var unknown
	 */
	protected $orders;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Response\UserBundle\Entity\UserGroup")
	 * @ORM\JoinTable(name="fos_user_group",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
	 * )
	 */
	public $groups;
	
	
	
	public function setSalt($salt) 
	{
		
		$this->salt = $salt;
		
	}	
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    	$this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add orders
     *
     * @param \Response\OrdersBundle\Entity\Orders $orders
     * @return User
     */
    public function addOrder(\Response\OrdersBundle\Entity\Orders $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Response\OrdersBundle\Entity\Orders $orders
     */
    public function removeOrder(\Response\OrdersBundle\Entity\Orders $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    

}
