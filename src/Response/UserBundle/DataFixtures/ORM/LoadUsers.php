<?php

namespace Response\OrdersBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
 use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Response\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$user = new User;
		$user->setUsername('man0l');
		$user->setEmail('manol.trendafilov@gmail.com');
		$user->addRole("ROLE_SUPER_ADMIN");
		$user->setEnabled(true);
		$user->setLocked(false);
		$user->setExpired(false);
		$user->setCredentialsExpired(false);
		
		$user->setSalt(md5(uniqid()));
		$user->setPlainPassword('1234');
		
		$encoder = $this->container
		->get('security.encoder_factory')
		->getEncoder($user)
		;
		$user->setPassword($encoder->encodePassword('1234', $user->getSalt()));
		
		
		$manager->persist($user);
		$manager->flush();
		
		$this->addReference('user', $user);
		
		$user = new User;
		$user->setUsername('man0l2');
		$user->setEmail('manol2.trendafilov@gmail.com');
		$user->addRole("ROLE_SUPER_ADMIN");
		$user->setEnabled(true);
		$user->setLocked(false);
		$user->setExpired(false);
		$user->setPlainPassword('1234');
		$user->setCredentialsExpired(false);
		
		$user->setSalt(md5(uniqid()));
		
		$encoder = $this->container
		->get('security.encoder_factory')
		->getEncoder($user)
		;
		$user->setPassword($encoder->encodePassword('1234', $user->getSalt()));
		
		
		$manager->persist($user);
		$manager->flush();
		
		$this->addReference('user2', $user);
	}
	
	public function getOrder() {
		
		return 1;
	}
	
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
	}
}
