<?php

namespace Response\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CategoriesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('parent', 'entity', array(
            		'class' => 'ResponseProductBundle:Categories',
            		'query_builder'=> function(EntityRepository $er){
            			$qb = $er->createQueryBuilder('c');
            			 
            			//return $qb->where("c.parent IS NULL");
            			return $qb;
            			 
            		}
            		
        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Response\ProductBundle\Entity\Categories'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'response_productbundle_categories';
    }
}
