<?php

namespace Response\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Categories
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Categories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Моля въведете заглавие")    
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    
    /**
     * @var ArrayCollection $subpages
     * @ORM\OneToMany(targetEntity="Categories", mappedBy="parent")
     */
    private $childs;
    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="Categories", inversedBy="childs")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Categories
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param string $parent
     *
     * @return Categories
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return string 
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * Constructor
     */	
    public function __construct()
    {
        //$this->parent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    
    public function __toString() 
    {
    	
    	$title = '';
    	if($this->parent)
    	{
    		if($this->parent->parent)
    		{
    			$title = $this->parent->parent;
    		}
    		
    		$title .= $this->parent;
    		$title .= ' -- '. $this->title;
    	} else 
    	{
    		$title .= $this->title;
    	}
    	
    	
    	
    	return $title;    	
    }

    /**
     * Add childs
     *
     * @param \Response\ProductBundle\Entity\Categories $childs
     * @return Categories
     */
    public function addChild(\Response\ProductBundle\Entity\Categories $childs)
    {
        $this->childs[] = $childs;

        return $this;
    }

    /**
     * Remove childs
     *
     * @param \Response\ProductBundle\Entity\Categories $childs
     */
    public function removeChild(\Response\ProductBundle\Entity\Categories $childs)
    {
        $this->childs->removeElement($childs);
    }

    /**
     * Get childs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChilds()
    {
        return $this->childs;
    }
}
