<?php

namespace Response\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Import
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Import
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\File(maxSize="6000000", 
     *       mimeTypes={
     *       	"text/csv",
     *       	"text/plain", 
     *          "text/comma-separated-values",
     *          "application/csv", 
     *          "application/excel", 
     *          "application/vnd.ms-excel", 
     *          "application/vnd.msexcel"},
     *        mimeTypesMessage="Моля изберете валиден CSV файл"  
     *          )
     * @Assert\NotBlank(message="Моля изберете CSV файл за качване, по малък от 6 мб")
     * 
     */
    private $file;
    
    /**
     * @ORM\Column(name="path", type="string", length=255)
     * @var unknown
     */
    public $path;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_parsed", type="boolean")
     */
    private $isParsed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    
    public function __construct() 
    {
		$this->createdAt = new \DateTime();    	
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Import
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set isParsed
     *
     * @param boolean $isParsed
     * @return Import
     */
    public function setIsParsed($isParsed)
    {
        $this->isParsed = $isParsed;

        return $this;
    }

    /**
     * Get isParsed
     *
     * @return boolean 
     */
    public function getIsParsed()
    {
        return $this->isParsed;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Import
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function setPath() 
    {
    		
    }
    
    public function getAbsolutePath()
    {
    	return null === $this->path
    	? null
    	: $this->getUploadRootDir().'/'.$this->path;
    }
    
    public function getWebPath()
    {
    	return null === $this->path
    	? null
    	: $this->getUploadDir().'/'.$this->path;
    }
    
    protected function getUploadRootDir()
    {
    	// the absolute directory path where uploaded
    	// documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw up
    	// when displaying uploaded doc/image in the view.
    	return 'uploads/csv';
    }
    
    public function upload() 
    {
    	// the file property can be empty if the field is not required
    	if (null === $this->getFile()) {
    		return;
    	}
    	
    	// use the original file name here but you should
    	// sanitize it at least to avoid any security issues
    	
    	// move takes the target directory and then the
    	// target filename to move to
    	$fileName = uniqid().'.csv';
    	$this->getFile()->move(
    			$this->getUploadRootDir(),
    			$fileName
    	);
    	
    	// set the path property to the filename where you've saved the file
    	//$this->path = $this->getFile()->getClientOriginalName();
    	$this->path = $fileName;
    	// clean up the file property as you won't need it anymore
    	$this->file = null;
    	 
    }
    
     

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
}
