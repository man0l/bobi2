<?php

namespace Response\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Products
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Моля въведете SKU номер")     
     * @ORM\Column(name="sku", type="string", length=255, nullable=true)
     */
    private $sku;

    /**
     * @var string
     * @Assert\NotBlank(message="Моля въведете заглавие")    
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank(message="Моля въведете описание")    
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;    

    /**
     * @ORM\ManyToOne(targetEntity="Brands")
     * @Assert\NotBlank(message="Моля изберете бранд")    
     * @ORM\JoinColumn(name="brand", referencedColumnName="id", nullable=true)
     */
    private $brand;
    
    /**
     * @ORM\ManyToOne(targetEntity="Products", inversedBy="childs")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @var unknown
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="Products", mappedBy="parent")
     * @var unknown
     */
    private $childs;  
    

    /**
     * @var \stdClass
     * 
     * @ORM\Column(name="collection", type="object", nullable=true)
     */
    private $collection;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Categories")
     * @Assert\NotBlank(message="Моля изберете категория")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=true)    
     * @var unknown
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Images", mappedBy="product")
     * @ORM\JoinColumn(name="images", referencedColumnName="id")
     */
    private $images;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="in_stock", type="integer", nullable=true)
     * @var unknown
     */
    private $inStock;
    
    /**
     * @ORM\ManyToOne(targetEntity="Size")
     * @Assert\NotBlank(message="Моля изберете размер")
     * @var unknown
     */
    private $size;
    
    /**
     * @ORM\ManyToOne(targetEntity="Color")
     * @Assert\NotBlank(message="Моля изберете цвят") 
     * @var unknown
     */
    private $color;
    
    /**
     * @ORM\Column(name="price", type="float", nullable=true)
     * @Assert\NotBlank(message="Моля въведете цена") 
     * @var unknown
     */
    private $price;
 
    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     * @var unknown
     */
    private $status;
    
    const STATUS_PUBLISHED = 1;
    const STATUS_NOTPUBLISHED = 2;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Products
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    
        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Products
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Products
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set options
     *
     * @param \stdClass $options
     *
     * @return Products
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return \stdClass 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set brand
     *
     * @param \stdClass $brand
     *
     * @return Products
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    
        return $this;
    }

    /**
     * Get brand
     *
     * @return \stdClass 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set collection
     *
     * @param \stdClass $collection
     *
     * @return Products
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    
        return $this;
    }

    /**
     * Get collection
     *
     * @return \stdClass 
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set images
     *
     * @param \stdClass $images
     *
     * @return Products
     */
    public function setImages($images)
    {
        $this->images = $images;
    
        return $this;
    }

    /**
     * Get images
     *
     * @return \stdClass 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Products
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Products
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime;
        $this->updatedAt = new \DateTime;
        
    }

    /**
     * Add options
     *
     * @param \Response\ProductBundle\Entity\Options $options
     *
     * @return Products
     */
    public function addOption(\Response\ProductBundle\Entity\Options $options)
    {
        $this->options[] = $options;
    
        return $this;
    }

    /**
     * Remove options
     *
     * @param \Response\ProductBundle\Entity\Options $options
     */
    public function removeOption(\Response\ProductBundle\Entity\Options $options)
    {
        $this->options->removeElement($options);
    }

    /**
     * Add category
     *
     * @param \Response\ProductBundle\Entity\Categories $category
     *
     * @return Products
     */
    public function addCategory(\Response\ProductBundle\Entity\Categories $category)
    {
        $this->category[] = $category;
    
        return $this;
    }

    /**
     * Remove category
     *
     * @param \Response\ProductBundle\Entity\Categories $category
     */
    public function removeCategory(\Response\ProductBundle\Entity\Categories $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param \Response\ProductBundle\Entity\Categories $category
     *
     * @return Products
     */
    public function setCategory(\Response\ProductBundle\Entity\Categories $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Add images
     *
     * @param \Response\ProductBundle\Entity\Images $images
     *
     * @return Products
     */
    public function addImage(\Response\ProductBundle\Entity\Images $images)
    {
        $this->images[] = $images;
    
        return $this;
    }

    /**
     * Remove images
     *
     * @param \Response\ProductBundle\Entity\Images $images
     */
    public function removeImage(\Response\ProductBundle\Entity\Images $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Set status
     *
     * @param \intege $status
     * @return Products
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \intege 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set inStock
     *
     * @param integer $inStock
     * @return Products
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;

        return $this;
    }

    /**
     * Get inStock
     *
     * @return integer 
     */
    public function getInStock()
    {
        return $this->inStock;
    }
    

    /**
     * Set parent
     *
     * @param \Response\ProductBundle\Entity\Products $parent
     * @return Products
     */
    public function setParent(\Response\ProductBundle\Entity\Products $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Response\ProductBundle\Entity\Products 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childs
     *
     * @param \Response\ProductBundle\Entity\Products $childs
     * @return Products
     */
    public function addChild(\Response\ProductBundle\Entity\Products $childs)
    {
        $this->childs[] = $childs;

        return $this;
    }

    /**
     * Remove childs
     *
     * @param \Response\ProductBundle\Entity\Products $childs
     */
    public function removeChild(\Response\ProductBundle\Entity\Products $childs)
    {
        $this->childs->removeElement($childs);
    }

    /**
     * Get childs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * Set size
     *
     * @param \Response\ProductBundle\Entity\Size $size
     * @return Products
     */
    public function setSize(\Response\ProductBundle\Entity\Size $size = null)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return \Response\ProductBundle\Entity\Size 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set color
     *
     * @param \Response\ProductBundle\Entity\Color $color
     * @return Products
     */
    public function setColor(\Response\ProductBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \Response\ProductBundle\Entity\Color 
     */
    public function getColor()
    {
        return $this->color;
    }
    
    public function getFirstImage()
    {
    	return $this->images[0];
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Products
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
	
	function getPriceLv()
	{
		if(strpos($this->price, ",") !== false)
		{
			list($lv,) = explode(",",$this->price);
		} else if(strpos($this->price, ".") !== false)
		{
			list($lv,) = explode(",",$this->price);
		} else $lv = $this->price;
	
	   return $lv;
	}
	
	function getPriceCoins()
	{
		if(strpos($this->price, ",") !== false)
		{
			list($lv, $coins) = explode(",",$this->price);
		} else if(strpos($this->price, "." ) !== false)
		{
			list($lv, $coins) = explode(",",$this->price);
		} else $coins = "00";
	
	   return $coins;
	}
}
