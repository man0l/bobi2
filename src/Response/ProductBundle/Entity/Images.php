<?php

namespace Response\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Images
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Images
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_order", type="integer")
     */
    private $displayOrder;
    
    /**
     * @ORM\ManyToOne(targetEntity="Products")
     * @ORM\JoinColumn(name="products", referencedColumnName="id")
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Images
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set displayOrder
     *
     * @param integer $displayOrder
     *
     * @return Images
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
    
        return $this;
    }

    /**
     * Get displayOrder
     *
     * @return integer 
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add product
     *
     * @param \Response\ProductBundle\Entity\Products $product
     *
     * @return Images
     */
    public function addProduct(\Response\ProductBundle\Entity\Products $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove product
     *
     * @param \Response\ProductBundle\Entity\Products $product
     */
    public function removeProduct(\Response\ProductBundle\Entity\Products $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set product
     *
     * @param \Response\ProductBundle\Entity\Products $product
     *
     * @return Images
     */
    public function setProduct(\Response\ProductBundle\Entity\Products $product = null)
    {
        $this->product = $product;
    
        return $this;
    }
}
