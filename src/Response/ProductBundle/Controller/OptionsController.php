<?php

namespace Response\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Response\ProductBundle\Entity\Options;
use Response\ProductBundle\Form\OptionsType;

/**
 * Options controller.
 *
 * @Route("/admin_options")
 */
class OptionsController extends Controller
{

    /**
     * Lists all Options entities.
     *
     * @Route("/", name="admin_options")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResponseProductBundle:Options')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Options entity.
     *
     * @Route("/", name="admin_options_create")
     * @Method("POST")
     * @Template("ResponseProductBundle:Options:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Options();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_options'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Options entity.
     *
     * @param Options $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Options $entity)
    {
        $form = $this->createForm(new OptionsType(), $entity, array(
            'action' => $this->generateUrl('admin_options_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Options entity.
     *
     * @Route("/new", name="admin_options_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Options();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Options entity.
     *
     * @Route("/{id}", name="admin_options_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Options')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Options entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Options entity.
     *
     * @Route("/{id}/edit", name="admin_options_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Options')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Options entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Options entity.
    *
    * @param Options $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Options $entity)
    {
        $form = $this->createForm(new OptionsType(), $entity, array(
            'action' => $this->generateUrl('admin_options_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Options entity.
     *
     * @Route("/{id}", name="admin_options_update")
     * @Method("PUT")
     * @Template("ResponseProductBundle:Options:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Options')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Options entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_options_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Options entity.
     *
     * @Route("/{id}", name="admin_options_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResponseProductBundle:Options')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Options entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_options'));
    }
    
    /**
     * Deletes a Options entity.
     *
     * @Route("/delete", name="admin_options_delete_all")
     * @Method("POST")
     */
    public function deleteAllAction(Request $request)
    {

    	$ids = $request->get('options_delete');
    	foreach($ids as $id) {
    		
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('ResponseProductBundle:Options')->find($id);
    		
    		$em->remove($entity);
    		$em->flush();
    		
    	}
    	
    	
    
    	return $this->redirect($this->generateUrl('admin_options'));
    }

    /**
     * Creates a form to delete a Options entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_options_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
