<?php

namespace Response\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Response\ProductBundle\Entity\Products;
use Response\ProductBundle\Form\ProductsType;
use Doctrine\Common\Collections\ArrayCollection;
use Response\ProductBundle\Entity\Images;
use Response\ProductBundle\Form\ImportType;
use Response\ProductBundle\Entity\Import;
use Response\ProductBundle\Entity\ImportedProducts;
 
/**
 * Products controller.
 *
 * @Route("/admin/products")
 */
class ProductsController extends Controller
{

    /**
     * Lists all Products entities.
     *
     * @Route("/", name="admin_products")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResponseProductBundle:Products')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Products entity.
     *
     * @Route("/create", name="admin_products_create")
     * @Method("POST")
     * @Template("ResponseProductBundle:Products:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Products();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
        	$em = $this->getDoctrine()->getManager();
        	$images = $request->get('images');
        	if($images) {
        		$imageCollection = new ArrayCollection();
        		foreach($images as $order=>$image) {
        			$productImage = new Images();
        			$productImage->setFile($image);
        			$productImage->setDisplayOrder($order);
        			$productImage->setProduct($entity);
        			$imageCollection->add($productImage);
        			
        			$em->persist($productImage);
        		}
        	}
        	
        	$entity->setImages($imageCollection);
        	
            
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_products', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Products entity.
     *
     * @param Products $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Products $entity)
    {
        $form = $this->createForm(new ProductsType(), $entity, array(
            'action' => $this->generateUrl('admin_products_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Products entity.
     *
     * @Route("/new", name="admin_products_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Products();
        $form   = $this->createCreateForm($entity);
        $timestamp = time();
        
		$salt = md5('unique_salt' . $timestamp);
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        	'salt'	=> $salt,
        	'timestamp' => $timestamp,
        	'file' => md5($timestamp)
        );
    }

    /**
     * Finds and displays a Products entity.
     *
     * @Route("/{id}", name="admin_products_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Products')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Products entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Products entity.
     *
     * @Route("/{id}/edit", name="admin_products_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Products')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Products entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        $timestamp = time();
        
        $salt = md5('unique_salt' . $timestamp);
        
        $images = $entity->getImages();
        
        if($images) {
        	foreach($images as $image) {
        		$files[] = $image->getFile();	
        	}
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        	'salt'	=> $salt,
        	'timestamp' => $timestamp,
        	'fileNames'	=> isset($files) ? $files : null
        			
        );
    }

    /**
    * Creates a form to edit a Products entity.
    *
    * @param Products $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Products $entity)
    {
        $form = $this->createForm(new ProductsType(), $entity, array(
            'action' => $this->generateUrl('admin_products_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Products entity.
     *
     * @Route("/{id}", name="admin_products_update")
     * @Method("PUT")
     * @Template("ResponseProductBundle:Products:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Products')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Products entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
        	
        	if($entity->getImages()) {
        		foreach($entity->getImages() as $image) {
        			
        			$em->remove($image);
        		}
        	}
        	
        	$images = $request->get('images');
        	if($images) {
        		$imageCollection = new ArrayCollection();
        		foreach($images as $order=>$image) {
        			$productImage = new Images();
        			$productImage->setFile($image);
        			$productImage->setDisplayOrder($order);
        			$productImage->setProduct($entity);
        			$imageCollection->add($productImage);
        			 
        			$em->persist($productImage);
        		}
        		
        		$entity->setImages($imageCollection);
        	}
        	 
        	
        	$em->persist($entity);
        	
            $em->flush();

            return $this->redirect($this->generateUrl('admin_products_edit', array('id' => $id)));
        }

        $timestamp = time();
        
        $salt = md5('unique_salt' . $timestamp);
        
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        	'fileNames'	  => $request->get('images'),
        	'salt'		  => $salt,
        	'timestamp'	  => $timestamp,
        	'file'		  => md5($timestamp)
        );
    }
    /**
     * Deletes a Products entity.
     *
     * @Route("/{id}", name="admin_products_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResponseProductBundle:Products')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Products entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_products'));
    }
    
    
    /**
     * Deletes a Products entity.
     *
     * @Route("/delete", name="admin_products_delete_all")
     * @Method("POST")
     */
    public function deleteAllAction(Request $request)
    {
    	$ids = $request->get('products_delete');
    	$em  = $this->getDoctrine()->getManager();
    	$rep = $em->getRepository('ResponseProductBundle:Products');
    	
     	
    	foreach($ids as $id) 
    	{  		
    		$entity = $rep->find($id);
			
    		switch($request->get('action')) 
    		{
    			case 'delete':
    			
    				$em->remove($entity);
    			break;
    			
    			case 'unmerge':
    				
    				$parent = $entity->getParent();
    				
    			
    				if($parent) {
    					 
	    				 
	    				$sql = "UPDATE Products SET parent_id = NULL WHERE id = :id";
	    				$conn = $em->getConnection();
	    				
	    				$stmt = $conn->prepare($sql);
	    				$stmt->bindValue("id", $id);
	    				
	    				$stmt->execute();
	    				
    				}
    				
    				break;

    			
    			default:
    				break;
    			
    			
    		}	
    	}
    	
    	$em->flush();
    	 
    
    	return $this->redirect($this->generateUrl('admin_products'));
    }

    /**
     * Creates a form to delete a Products entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_products_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * @Route("/upload-image", name="product_upload_image")
     * @Template()
     */
    public function uploadImageAction() {
    	
    	$request = $this->getRequest();
    	$uploadedFile = $request->files->get('Filedata');
    	
    	$file = sprintf("%s.%s", md5(time()), $uploadedFile->guessExtension());
    	
    	$dir = str_replace("/app", "/web/uploads/products", $this->get('kernel')->getRootDir());
    	
    	$uploadedFile->move($dir, $file);  
    	$fileNames[] = $file;
    	
    	return array('fileNames' => $fileNames);
    }
    
    /**
     * @Route("/import/csv", name="importcsv")
	 * @Template()
     */
    public function importCSVAction(Request $request) {
    	
    	$entity = new Import();
    	$form = $this->createForm(new ImportType(), $entity, array(
    			'action' => $this->generateUrl('importcsv'),
    			'method' => 'POST',
    	));
    	
    	$form->handleRequest($request);
    	 
    	
    	if ($form->isValid()) {
    		 		
    		$entity->upload();
    		$entity->setIsParsed(false);
    		
    		$em = $this->getDoctrine()->getManager();
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		// parse csv
    		$i = 0;
    		$ignoreFirstLine = true;
    		if($handle = fopen($this->container->get('kernel')->getRootDir()."/../web/uploads/csv/".$entity->getPath(), "r")) {
	    		while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
	    			$i++;
	    			if ($ignoreFirstLine && $i == 1) { continue; }
	    			$rows[] = $data;
	    		}
	    		fclose($handle);    		 
    		}
    		
    		foreach($rows as $row) {
    			
    			$imported = new ImportedProducts();
    			$imported->setTitle($row[0]);
    			$imported->setSku($row[1]);
    			$imported->setQuantity($row[2]);
    			
    			$em->persist($imported);
    			
    			$product = new Products;
    			$product->setSku($row[0]);
    			if($row[2]) {
    				print_r($row[2]);
	    			preg_match("!(\d+?)\s..\.!", $row[2], $match);
	    			
	    			$qty = intval(trim($match[1]));
	    			
	    			if($qty)
	    			{
	    				$product->setInStock(1);
	    			}
    			
    			}
    			
    			$product->setStatus(Products::STATUS_NOTPUBLISHED);
    			
    			$em->persist($product);
    			
    			$em->flush();
    			
    		}
    		
    		$this->get('session')->getFlashBag()->add('notice', 'Добавихте файла успешно.');
    		
    		
    	}
    	 
		return array('form' => $form->createView());
    }
    
    /**
     * @Route("/merge/popup", name="merge_popup")
     * @Template("ResponseProductBundle:Products:merge_popup.html.twig")
     */
    public function mergePopupAction()
    {
    	$ids = $this->getRequest()->get('ids');
    	$rep = $this->getDoctrine()->getRepository("ResponseProductBundle:Products");
    	// select products with these ids
    	foreach($ids as $id) {
    		$products[] = $rep->findOneBy(array('id' => $id));
    	}
    	
    	return array('products' => $products);
    	
    			
    }
    
    /**
     * @Route("/merge/form-action", name="merge_form_action")
     * @Template("ResponseProductBundle:Products:merge_form.html.twig")
     */
    public function mergeFormAction(Request $request) 
    {
    	$mainProductId = $this->getRequest()->get('main_product_id');
    	$rep = $this->getDoctrine()->getRepository("ResponseProductBundle:Products");
    	$em = $this->getDoctrine()->getManager();
    	
    	$product = $rep->find($mainProductId);
    	
    	$ids = $request->get('products');
    	
    	foreach($ids as $id)
    	{
    		if($mainProductId !== $id) {	
	    		$childProduct = $rep->find($id);
	    		$childProduct->setParent($product);
	    		$em->persist($product);
    		}
    	}
    	
    	$em->flush();
    	
    	/* $response = new Response();
    	return $response; */
    	return array();
    }
    
}
