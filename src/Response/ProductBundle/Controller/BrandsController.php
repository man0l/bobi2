<?php

namespace Response\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Response\ProductBundle\Entity\Brands;
use Response\ProductBundle\Form\BrandsType;

/**
 * Brands controller.
 *
 * @Route("/admin_brands")
 */
class BrandsController extends Controller
{

    /**
     * Lists all Brands entities.
     *
     * @Route("/", name="admin_brands")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResponseProductBundle:Brands')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Brands entity.
     *
     * @Route("/create", name="admin_brands_create")
     * @Method("POST")
     * @Template("ResponseProductBundle:Brands:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Brands();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_brands'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Brands entity.
     *
     * @param Brands $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Brands $entity)
    {
        $form = $this->createForm(new BrandsType(), $entity, array(
            'action' => $this->generateUrl('admin_brands_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Brands entity.
     *
     * @Route("/new", name="admin_brands_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Brands();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Brands entity.
     *
     * @Route("/{id}", name="admin_brands_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Brands')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brands entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Brands entity.
     *
     * @Route("/{id}/edit", name="admin_brands_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Brands')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brands entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Brands entity.
    *
    * @param Brands $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Brands $entity)
    {
        $form = $this->createForm(new BrandsType(), $entity, array(
            'action' => $this->generateUrl('admin_brands_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Brands entity.
     *
     * @Route("/{id}", name="admin_brands_update")
     * @Method("PUT")
     * @Template("ResponseProductBundle:Brands:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseProductBundle:Brands')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brands entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_brands_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Brands entity.
     *
     * @Route("/{id}", name="admin_brands_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ResponseProductBundle:Brands')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Brands entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_brands'));
    }
    
    /**
     * Deletes a Brands entity.
     *
     * @Route("/delete", name="admin_brands_delete_all")
     * @Method("POST")
     */
    public function deleteAllAction(Request $request)
    {
    	$ids = $request->get('brands_delete');
    	
    	foreach($ids as $id) {
    		$em = $this->getDoctrine()->getManager();  		 
    		$entity = $em->getRepository('ResponseProductBundle:Brands')->find($id);
    		$em->remove($entity);
    		$em->flush();    	
    	}
    
    	return $this->redirect($this->generateUrl('admin_brands'));
    }
    

    /**
     * Creates a form to delete a Brands entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_brands_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
