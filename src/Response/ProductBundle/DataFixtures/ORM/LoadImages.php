<?php

namespace Response\ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Response\ProductBundle\Entity\Images;

class LoadImages extends AbstractFixture implements OrderedFixtureInterface {
	
	
	public function load(ObjectManager $manager) {
		 
		$image = new Images;
		$image->setFile("0af7c7b900cb60ff4add7db337346f59.jpeg");
		$image->setDisplayOrder(1);
		$image->setProduct($this->getReference('product1'));
		$manager->persist($image);		
		$manager->flush();
		$this->addReference('image1', $image);
		
		$image = new Images;
		$image->setFile("2766dfc504078e56b4274e87d2292a44.jpeg");
		$image->setDisplayOrder(2);
		$image->setProduct($this->getReference('product1'));
		$manager->persist($image);
		$manager->flush();
		$this->addReference('image2', $image);
		
		$image = new Images;
		$image->setFile("06b6b09fb54399e1cbf6285aec94d8df.jpeg");
		$image->setDisplayOrder(3);
		$image->setProduct($this->getReference('product1'));
		$manager->persist($image);
		$manager->flush();
		$this->addReference('image3', $image);	

		/* images for product 2 */
		$image = new Images;
		$image->setFile("0af7c7b900cb60ff4add7db337346f59.jpeg");
		$image->setDisplayOrder(1);
		$image->setProduct($this->getReference('product2'));
		$manager->persist($image);
		$manager->flush();
		$this->addReference('image4', $image);
		
		$image = new Images;
		$image->setFile("2766dfc504078e56b4274e87d2292a44.jpeg");
		$image->setDisplayOrder(2);
		$image->setProduct($this->getReference('product2'));
		$manager->persist($image);
		$manager->flush();
		$this->addReference('image5', $image);
		
		$image = new Images;
		$image->setFile("06b6b09fb54399e1cbf6285aec94d8df.jpeg");
		$image->setDisplayOrder(3);
		$image->setProduct($this->getReference('product2'));
		$manager->persist($image);		
		$manager->flush();
		$this->addReference('image6', $image);
		 
	}
	
	public function getOrder() {
		return $this->order = 6;
	}
}