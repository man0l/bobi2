<?php

namespace Response\ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Response\ProductBundle\Entity\Brands;

class LoadBrands extends AbstractFixture implements OrderedFixtureInterface {
	
	
	public function load(ObjectManager $manager) {
		 
		$brand = new Brands();
		$brand->setTitle('Kawasaki');		
		$manager->persist($brand);
		$manager->flush();
		$this->addReference('brand1', $brand);
		
		$brand = new Brands();
		$brand->setTitle('Yamaha');
		$manager->persist($brand);
		$manager->flush();
		$this->addReference('brand2', $brand);
		
		$brand = new Brands();
		$brand->setTitle('Ducatti');
		$manager->persist($brand);
		$manager->flush();
		$this->addReference('brand3', $brand);
		
		
	}
	
	public function getOrder() {
		return $this->order = 2;
	}
}