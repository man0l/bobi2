<?php

namespace Response\ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Response\ProductBundle\Entity\Options;

class LoadOptions extends AbstractFixture implements OrderedFixtureInterface {
	
	
	public function load(ObjectManager $manager) {

		$opt = new Options();
		$opt->setTitle('Цвят');
		$opt->setValue('Жълт');		
		
		$manager->persist($opt);
		$manager->flush();
		$this->addReference('option1', $opt);
		
		$opt = new Options();
		$opt->setTitle('Цвят');
		$opt->setValue('Зелен');
		$manager->persist($opt);
		$manager->flush();
		$this->addReference('option2', $opt);
		
		$opt = new Options();
		$opt->setTitle('Цвят');
		$opt->setValue('Светлосин');
		$manager->persist($opt);
		$manager->flush();
		$this->addReference('option3', $opt);		
	}
	
	public function getOrder() {
		return $this->order = 4;
	}
}