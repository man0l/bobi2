<?php

namespace Response\ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use Response\ProductBundle\Entity\Categories;


class LoadCategory extends AbstractFixture implements OrderedFixtureInterface {
	
	
	public function load(ObjectManager $manager) {
		
		 $category = new Categories();
		 $category->setTitle("Каски за мотори");		 
		 $manager->persist($category);
		 $manager->flush();		 
		 $this->addReference("category1", $category);
		 
		 $category = new Categories();
		 $category->setTitle("Мотори");		 	
		 $manager->persist($category);
		 $manager->flush();		 	
		 $this->addReference("category2", $category);
		 
		 $category = new Categories();
		 $category->setTitle("Части за мотори");		 	
		 $manager->persist($category);
		 $manager->flush();		 	
		 $this->addReference("category3", $category);
	}
	
	public function getOrder() {
		return $this->order = 3;
	}
}