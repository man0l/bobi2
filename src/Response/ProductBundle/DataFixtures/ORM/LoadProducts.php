<?php

namespace Response\ProductBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Response\ProductBundle\Entity\Products;
use Doctrine\Common\Collections\ArrayCollection;


class LoadProducts extends AbstractFixture implements OrderedFixtureInterface {
	
	
	public function load(ObjectManager $manager) {
		
		$options = new ArrayCollection();
		$options->add($this->getReference('option1'));
		$options->add($this->getReference('option2'));
		$options->add($this->getReference('option3'));
		
		/* $images = new ArrayCollection();
		$images->add($this->getReference('image1'));
		$images->add($this->getReference('image2'));
		$images->add($this->getReference('image3')); */
		
		$product = new Products();
		$product->setCategory($this->getReference('category1'));
		$product->setOptions($options);
		$product->setTitle('Каска за мотор');
		$product->setDescription("Стандартна каска за мотор за хора над 18 годишна възраст.");
		//$product->setImages($images);
		$product->setBrand($this->getReference('brand1'));
		$product->setSku("11456ffjk");
		
		$manager->persist($product);
		$manager->flush();
		
		$this->addReference('product1', $product);
		
		/* product #2 */
		
		$options = new ArrayCollection();
		$options->add($this->getReference('option1'));
 		$options->add($this->getReference('option3'));
		
	/* 	$images = new ArrayCollection();
		$images->add($this->getReference('image1'));
		$images->add($this->getReference('image2'));
		$images->add($this->getReference('image3')); */
		
		$product = new Products();
		$product->setCategory($this->getReference('category1'));
		$product->setOptions($options);
		$product->setTitle('Каска за мотор 2');
		$product->setDescription("Стандартна каска за мотор 2 за хора над 18 годишна възраст.");
		//$product->setImages($images);
		$product->setBrand($this->getReference('brand2'));
		$product->setSku("11456ffjijk");
		
		
		$manager->persist($product);
		$manager->flush();
		
		$this->addReference('product2', $product);
		
	}
	
	public function getOrder() {
		return $this->order = 5;
	}
}