<?php

namespace Response\OrdersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItems
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class OrderItems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *      
     * @ORM\ManyToOne(targetEntity="Orders", inversedBy="orderItems")
     * @ORM\JoinColumn(name="orders", referencedColumnName="id")
     */
    private $orders;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\Response\ProductBundle\Entity\Products")
     * @ORM\JoinColumn(name="products", referencedColumnName="id") 
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orders
     *
     * @param integer $orders
     *
     * @return OrderItems
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    
        return $this;
    }

    /**
     * Get orders
     *
     * @return integer 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return OrderItems
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    
        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set product
     *
     * @param \Response\ProductBundle\Products $product
     *
     * @return OrderItems
     */
    public function setProduct(\Response\ProductBundle\Entity\Products $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return \Response\ProductBundle\Products 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
