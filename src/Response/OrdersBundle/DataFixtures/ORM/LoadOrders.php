<?php

namespace Response\OrdersBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Response\OrdersBundle\Entity\Orders;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;


class LoadOrders extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		$order = new Orders;
		$order->setTotal(22.5);
		$order->setCreatedAt(new \Datetime());
		$order->setUser($this->getReference('user'));				
		
		
		$manager->persist($order);
		$manager->flush();
		
		$this->addReference('order', $order);
		
		$order = new Orders;
		$order->setTotal(200.5);
		$order->setCreatedAt(new \Datetime());
		$order->setUser($this->getReference('user2'));
		
		
		$manager->persist($order);
		$manager->flush();
		
		$this->addReference('order2', $order);
	}
	
	public function getOrder() {
		
		return 7;
	}
}
