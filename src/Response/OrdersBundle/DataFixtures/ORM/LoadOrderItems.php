<?php

namespace Response\OrdersBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Response\OrdersBundle\Entity\OrderItems;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadOrderItems extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function load(ObjectManager $manager)
	{
		

		$oi = new OrderItems();
		$oi->setOrders($this->getReference('order'));
		
		$oi->setProduct($this->getReference('product1'));
		
		
		$manager->persist($oi);
		$manager->flush();
		
		/* order items #2, same order*/
		
		$oi = new OrderItems();
		$oi->setOrders($this->getReference('order'));		
		$oi->setProduct($this->getReference('product2'));		
		
		$manager->persist($oi);
		$manager->flush();
		
		/* new order, product 2 only */
		
		$oi = new OrderItems();
		$oi->setOrders($this->getReference('order2'));
		$oi->setProduct($this->getReference('product2'));
		
		$manager->persist($oi);
		$manager->flush();
		 
	}
	
	public function getOrder() {
		
		return 8;
	}
	
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
	}
	
	
	
}
