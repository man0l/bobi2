<?php


namespace Response\APIBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;


class OrdersController  extends ContainerAware
{
	

	function getAction() {
		
		$statusCode = Response::HTTP_OK; 
		// the firewall must require a logged user
		$securityContext = $this->container->get('security.context');
		
		
		if($securityContext->isGranted('ROLE_USER')) {
		
			$user	 = $this->container->get('security.context')->getToken()->getUser();					
			$rep	 = $this->container->get('doctrine')->getManager()->getRepository('ResponseOrdersBundle:Orders');
			$orders	 = $rep->findBy(array('user' => $user), array('createdAt' => 'DESC'));			
			$vars	 =  array('orders' => $orders);
			
		} else { 
			$vars = array('error' => 'Unauthorized access');
			$statusCode = Response::HTTP_UNAUTHORIZED;
		}
		
		$view = View::create($vars, $statusCode);
		
		return $view;
	}
	
	
	function viewAction($id) {
		
		$statusCode = Response::HTTP_OK;	
		 
		$securityContext = $this->container->get('security.context');		
		
		if($securityContext->isGranted('ROLE_USER')) {
		
			$user = $this->container->get('security.context')->getToken()->getUser();
			
			$rep = $this->container->get('doctrine')->getManager()->getRepository('ResponseOrdersBundle:Orders');
			$order = $rep->findBy(array('user' => $user, 'id' => $id), array('createdAt' => 'DESC'));
			
			$vars = array('order' => $order);
		
		} else {
			$statusCode = Response::HTTP_UNAUTHORIZED;
			$vars = array('error' => "Unauthorized access");
		}
		
		$view = View::create($vars, $statusCode);
		
		return $view;
		
	} 
}