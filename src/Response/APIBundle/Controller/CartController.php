<?php

namespace Response\APIBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Session;


class CartController  extends ContainerAware
{
	
	function __construct() {
		
		$this->session = new Session(new MockArraySessionStorage());
		$this->session->start();
	}
	
	/**
	 * @Rest\View()
	 */
	function getAction(Request $request) {
		//$session = $this->container->get('session');
		
		$cart = $this->session->get('cart');
		
		print_r($cart);
		return array('cart' => $cart);
	}
	

	function addItemAction(Request $request) {
		
		//$session = $this->container->get('session');
		
		$cart = array();
		
		if($this->session->has('cart')) {
			$cart = $this->session->get('cart');
		} 
		
		$productID = $request->get('product_id');
		$rep = $this->container->get('doctrine')->getManager()->getRepository('ResponseProductBundle:Products');
		$product = $rep->find($productID);
		
		$view = View::create();
		
		if($product) {
			$cart[$productID] = $request->get('qty');
			
			$this->session->set('cart', $cart);	
		
			$view->setData(array('cart' => $cart));
			$view->setStatusCode(Response::HTTP_OK);
			
			
		} else {
			$view->setData(array());
			$view->setStatusCode(Response::HTTP_NOT_FOUND);
		}
		
		return $view;
		
	}
	
	function updateItemAction(Request $request) {

		//$session = $this->container->get('session');
		
		$cart = $this->session->get('cart');
		
		$productID = $request->get('product_id');
		$rep = $this->container->get('doctrine')->getManager()->getRepository('ResponseProductBundle:Products');
		$product = $rep->find($productID);
		
		
		$cart[$productID] = $request->get('qty');
		
		$this->session->set('cart', $cart);		
		
		$view = View::create();
		
		if($product) {
			$view->setData(array('cart' => $cart));
			$view->setStatusCode(Response::HTTP_OK);			
		} else {
			$view->setData(array());
			$view->setStatusCode(Response::HTTP_NOT_FOUND);
		}
		
		return $view;
		
	}
	
	
	function deleteItemAction(Request $request) {
	
		//$session = $this->container->get('session');
		$cart = $this->session->get('cart');
		$productID = $request->get('product_id');
		
		$view = View::create();
		
		if($cart[$productID]) {
			
		unset($cart[$productID]);
		
		$this->session->set('cart', $cart);
		
		
		$view->setData(array('cart' => $cart));
		$view->setStatusCode(Response::HTTP_OK);
		
		} else {
			$view->setData(array());
			$view->setStatusCode(Response::HTTP_NOT_FOUND);
		}
		return $view;
		
		
	}
	
}