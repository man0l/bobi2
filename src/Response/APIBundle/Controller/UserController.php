<?php

namespace Response\APIBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Response\APIBundle\Form\Type\LoginFormType;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;


class UserController  extends ContainerAware
{
 
	public function loginAction(Request $request) {
		
	 	$statusCode = 200;
	 	
		$form = $this->container->get('form.factory')->create(new LoginFormType());
		
		$form->bind($request);
				
		$data = $form->getData();

		$constraintsArray['username'] = new NotBlank(array('message' => 'Моля въведете потребител'));
		$constraintsArray['password'] = new NotBlank(array('message' => 'Моля въведете парола'));
		
		$constraints = new Collection($constraintsArray);
		
		$errorList = $this->container->get('validator')->validateValue($data, $constraints);
			
		if (count($errorList) == 0) {
			
			$um = $this->container->get('fos_user.user_manager');
			$user = $um->findUserByUsername($data['username']);
			
			$encoder = $this->container->get('security.encoder_factory');
			$encoder = $encoder->getEncoder($user);
			$encodedPass = $encoder->encodePassword($data['password'], $user->getSalt());
				
			
			if($user && ($user->getPassword() == $encodedPass)) {
				$token = new UsernamePasswordToken($data['username'], $data['password'], 'main');
				$this->container->get("security.context")->setToken($token);
				
				$event = new InteractiveLoginEvent($request, $token);
				$this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event); 
			} else $statusCode = 404;
			
		}
		
		$view = View::create(array());
		// insecure! if the password do not match, the user is showed up
		//$view->setData(array('user' => $user));
		
		$view->setStatusCode($statusCode);
		
		return $view;
		
		 		
	}
	
	
	public function getAction(Request $request) {
		
		$securityContext = $this->container->get('security.context');		
		
		if($securityContext->isGranted('ROLE_USER')) {
			
			$user = $this->container->get("security.context")->getToken()->getUser();
			$um = $this->container->get('fos_user.user_manager');
			$user = $um->findUserByUsername($user);
			$vars = array('user' => $user);			
				
		} else {
			$vars = array('error' => 'Unauthorized access');
			$statusCode = Response::HTTP_UNAUTHORIZED;
		}
		
		
		$view = View::create($vars, $statusCode);
		return $view;
		
	}
	
	
	/**
	 * @Rest\View()
	 */
	public function deleteAction(Request $request) {
		
		$anonToken = new AnonymousToken('TheKeyIsNotSet', 'anon.', array());
		$this->container->get('security.context')->setToken($anonToken);	

		return array();
 		
	}

	 
}
