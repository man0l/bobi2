<?php

namespace Response\APIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginFormType extends AbstractType
{
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('username');
        $builder->add('password', 'password');
		
	}

	
	
	public function getName()
	{
		return 'user';		
	}
	
}
