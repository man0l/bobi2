<?php

namespace Response\APIBundle\Tests\Controller;

use Response\APIBundle\Tests\MyWebTestCase;

class UserControllerTest extends MyWebTestCase
{
   
    public function testLogin()
     { 
     	
     	$params = array( 'user' => array(
     			'username' => 'man0l',
     			'password'	=> '1234'
     	
     		)
     	);
     	
		$client   = static::createClient();
		$crawler  = $client->request('PUT', '/api/user/login', $params);
		
		
		$response = $client->getResponse();
		
		$this->assertJsonResponse($response, 200);

     }
     

     public function testLogged() {
     	
     	$client   = static::createClient();
     	$crawler  = $client->request('GET', '/api/user');
     	
     	
     	$response = $client->getResponse();
     	
     	$this->assertJsonResponse($response, 200);
     	
     	
     }
     
     public function testDelete() {
     
     	$client   = static::createClient();
     	$crawler  = $client->request('DELETE', '/api/user/delete');
     
     
     	$response = $client->getResponse();
     
     	$this->assertJsonResponse($response, 200);
     
     
     }
     
    
      
}
