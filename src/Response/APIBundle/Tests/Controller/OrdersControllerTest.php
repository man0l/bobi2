<?php

namespace Response\APIBundle\Tests\Controller;

use Response\APIBundle\Tests\MyWebTestCase;

class OrdersControllerTest extends MyWebTestCase
{
   
    function testGet() {

    	// test the security firewall
    	$client   = static::createClient();
    	$crawler  = $client->request('GET', '/api/user/orders/get');
    	
    	
    	$response = $client->getResponse();
    	
    	$this->assertJsonResponse($response, 200);
    	
    }
    
    function testView() {
    	
    	$client   = static::createClient();
    	$crawler  = $client->request('GET', '/api/user/orders/get/1');
    	 
    	 
    	$response = $client->getResponse();
    	 
    	$this->assertJsonResponse($response, 200);
    	 
    	
    }
     
   
     
    
      
}
