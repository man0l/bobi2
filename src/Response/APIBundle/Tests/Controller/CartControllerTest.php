<?php


namespace Response\APIBundle\Tests\Controller;

use Response\APIBundle\Tests\MyWebTestCase; 

class CartControllerTest extends MyWebTestCase
{
	
	
	function testAdd() {
		

		$params = array(  
				'product_id' => 2,
				'qty' => 1
		);
		
		$client   = static::createClient();
		$crawler  = $client->request('PUT', '/api/cart/add', $params);
		
		
		$response = $client->getResponse();
		
		$this->assertJsonResponse($response, 200);
		
	}
	
 
	function testGet() {
		
		$params = array();
				 
		
		$client   = static::createClient();
		$crawler  = $client->request('GET', '/api/cart', $params);
		
		
		$response = $client->getResponse();		
		$this->assertJsonResponse($response, 200);
		
	}
	 
	function testUpdate() {
		
		$params = array(
				'product_id' => 2,
				'qty' => 2
		);
		
		$client   = static::createClient();
		$crawler  = $client->request('POST', '/api/cart/update', $params);
		
		
		$response = $client->getResponse();
		
		$this->assertJsonResponse($response, 200);
		
		
	}
	 
	function testDelete() {
		
		$params = array(
				'product_id' => 1,
				'qty' => 0
		);
			
		
		$client   = static::createClient();
		$crawler  = $client->request('DELETE', '/api/cart/delete', $params);
		
		
		$response = $client->getResponse();
		$this->assertJsonResponse($response, 200);
		
	}
	
	
	
}