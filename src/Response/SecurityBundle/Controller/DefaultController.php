<?php

namespace Response\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/login", name="_security_login")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/admin/check", name="_security_check")
     *
     */
    public function checkAction()
    {
    
    }
    
    /**
     * @Route("/admin/logout", name="_security_logout")
     */
    public function logoutAction() {
    	
    	
    }
}
