BobiMX Installation
========================

This is development preview of BobiMX Shopping cart site.

1) Installing the BobiMX
----------------------------------

All you have to do is to run clone the project through GIT and to execute two symfony2 commands from the command line.

### Clonning the project

Make a directory project through the cmd to your web server and execute the following command: 

    git clone https://man0l@bitbucket.org/man0l/bobimx.git

### Edit database settings and create database
After that, you have to edit the database settings. The file with the db settings in app/config/parameters.yml. 
It is in YML format ( use spaces, no tabs allowed ).
Then, use the `doctrine:schema:create` command to create a database through the console:

    php app/console doctrine:schema:create

This command will install the architecture through the SQL command CREATE TABLE syntax.

### Load testing data

For quick start, you have to run the command `doctrine:fixtures:load`. This commands inserts users, products and orders.
Run the following command:

    php app/console doctrine:fixtures:load
    
The default insterted user is: man0l / 1234. The Login page is http://<path/to/project>/web/app_dev.php/admin

This is all! Enjoy the new app